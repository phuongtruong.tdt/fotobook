require 'rails_helper'

RSpec.describe Photo, type: :model do
  describe "validations" do
    it "is valid with valid attributes" do
        expect(Photo.new).to be_valid
      end
    
    it "has a valid factory" do
        expect(FactoryGirl.build(:photo).save).to be_truthy
    end

    it "is title nil" do
        expect(FactoryGirl.build(:photo, title: nil).save).to be_falsey
    end

    it "is description nil" do
         expect(FactoryGirl.build(:photo, description: nil).save).to be_falsey
    end

    it "is private nil" do
         expect(FactoryGirl.build(:photo, private: nil).save).to be_falsey
    end

    it "is user id nil" do
         expect(FactoryGirl.build(:photo, user_id: nil).save).to be_falsey
    end

    it "is Enough info public" do
        expect(FactoryGirl.build(:photo, private: true).save).to be_truthy
    end

    it "is Enough info private" do
         expect(FactoryGirl.build(:photo, private: false).save).to be_truthy
    end

  end
end
