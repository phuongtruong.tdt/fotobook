require 'rails_helper'

RSpec.describe Album, type: :model do
    it "is valid with valid attributes" do
        expect(Album.new).to be_valid
      end
    
    it "has a valid factory" do
        expect(FactoryGirl.build(:album).save).to be_truthy
    end

    it "is title nil" do
        expect(FactoryGirl.build(:album, title: nil).save).to be_falsey
    end

    it "is description nil" do
         expect(FactoryGirl.build(:album, description: nil).save).to be_falsey
    end

    it "is private nil" do
         expect(FactoryGirl.build(:album, private: nil).save).to be_falsey
    end

    it "is user id nil" do
         expect(FactoryGirl.build(:album, user_id: nil).save).to be_falsey
    end

    it "is Enough info public" do
        expect(FactoryGirl.build(:album, private: true).save).to be_truthy
    end

    it "is Enough info private" do
         expect(FactoryGirl.build(:album, private: false).save).to be_truthy
    end

end
