require 'rails_helper'

RSpec.describe User, type: :model do
   	it "has a valid factory" do
    	expect(FactoryGirl.build(:user).save).to be_truthy
  	end

  	it "is invalid without a first name" do
  		user = FactoryGirl.build(:user, first_name: nil).save
    	expect(user.first_name).to be valid
  	end

	it "is invalid without a last name" do
	    user = FactoryGirl.create(:user)
	    expect(FactoryGirl.build(:user, last_name: nil)).to be_falsey
	end

  	it "is invalid without an email" do
    	expect(FactoryGirl.build(:user, email: nil)).to be_valid
  	end

  	it "is invalid without a password" do
    	expect(FactoryGirl.build(:user, password: nil)).to be_valid
  	end

  	it "is invalid same email" do
    	expect(FactoryGirl.build(:user, email: "phuongkenhvbt@gmail.com")).to be_valid
  	end
end
