# NUS REVIEW CODE RESULT FOR FINAL APP

### Issues
---------

#### Architecture



#### Security

1/ With current implementation, am user can easily access the admin module and manage the system data.

#### Performance


#### Coding conventions & best practices

1/ Need to have validation on both of frontend and backend --done

  - Login
  - Signup
  - Forgot pass
  - Etc.


2/ User active record validation to valiate instead of use if-else by yourself.

  - app/controllers/albums_controller.rb#update
  - app/controllers/photos_controller.rb
  - app/controllers/admin/...


3/ In controller, method `render` will render template by default. We do not need to specify `template`. --done


4/ If we do not have action `index` for a resource, we do not need to define it in the routes. It's very weird when you try to render album from in action index. It's follow the RESTful.

  - app/controllers/albums_controller.rb  -- done 
  - app/controllers/photos_controller.rb


5/ We can use kaminari to get total size of resource in database. We don't need to declare new variable for total size. -- done

  - app/controllers/users_controller.rb

    + @album = check_album().distinct.page(params[:page_album]).per(8)

  => We can get total size of album by `@album.total_count`.


6/ Name of variable which have a value is collection of something should be in plural -- done

  - app/controllers/users_controller.rb

    + @album = check_album().distinct.page(params[:page_album]).per(8)#show

  => it should be `@albums`


7/ Do not use parentheses after empty argument method. --done

  - app/controllers/users_controller.rb#show

    + @album = check_album().distinct.page(params[:page_album]).per(8)

  => should be: `@albums = check_album.distinct.page(params[:page_album]).per(8)`


8/ Unclear logic --done

  - app/controllers/users_controller.rb#show

  => Why we need to call `check_album` every time we access user's albums? It will make the app slower.


9/ Do not commit the files which are not in used --done

  - app/helpers/...


10/ Do not use instance variable in partial  --done

  - app/views/albums/_list_albums.html.erb
  - etc.


11/ Use I18n to replace hard code text.


12/ Missing unit tests.


### Bugs

1/ `:images.size <= 25` => it's a always TRUE condition. Why do you do that? Please note that the `:images.size` is return the size/length of the symbol `images`. It does not make sense.

2/ The UI is not good.

