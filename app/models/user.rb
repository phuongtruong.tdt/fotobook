class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable
    devise :omniauthable, :omniauth_providers => [:facebook]

 	 has_many :albums, dependent: :destroy
	 has_many :photos, dependent: :destroy

   has_one_attached :avatar

	 has_many :likes, dependent: :destroy
   has_many :follower_relations, dependent: :destroy, class_name: Relation.name, foreign_key: :follower_id # the relation in which the User is the follower
   has_many :following_relations, dependent: :destroy, class_name: Relation.name, foreign_key: :following_id # the relation in which the User is the followee

   has_many :followers, through: :following_relations, dependent: :destroy, source: :follower # the ones following you
   has_many :following, through: :follower_relations, dependent: :destroy, source: :following # the ones youre

   #create scope
   scope :new_first, -> { order(created_at: :desc)}
   
  #  def self.from_omniauth(auth)
  #    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
  #      user.provider = auth.provider
  #      user.uid = auth.uid
  #      user.email = auth.info.email
  #      user.password = Devise.friendly_token[0,20]
  #    end
  # end

  validates :first_name, :last_name,  presence: true
  validates :email, uniqueness: true
  validates :first_name, :last_name, length: {in: 1..25}

  def full_name
    "#{first_name}" + " " + "#{last_name}"
  end

  def follow(user_id)
    follower_relations.create(following_id: user_id)
  end

  def unfollow(user_id)
    follower_relations.find_by(following_id: user_id).destroy
  end

  protected
  def send_devise_notification(notification, *args)
    message = devise_mailer.send(notification, self, *args)
    message.deliver_now
  end
end