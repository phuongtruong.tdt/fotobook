class Relation < ApplicationRecord
	  # When A follows B then: A is the follower and B is the followee
  belongs_to :follower, class_name: User.name
  belongs_to :following, class_name: User.name
end
