class RelationsController < ApplicationController
	before_action :authenticate_user!
	before_action :set_user

	def follow_user
		current_user.follow params[:id]
		flash[:notice] = "Successfully follow: #{@user.full_name}"
		redirect_to current_user
	end

	def unfollow_user 
	    current_user.unfollow params[:id]
	    flash[:notice] = "Successfully unollow: #{@user.full_name}"
		redirect_to current_user
	end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end
end
