class Admin::PhotosController < ApplicationController
	before_action :authenticate_user!
	before_action :set_photo, only: [:index, :edit, :update, :destroy]

	def index
 		@photo = Photo.all.new_first.page(params[:page]).per(8)
 	end

	def edit
	end

	def update
		if params[:photo][:image] != nil
			@photo_admin.update(title: params[:photo][:title], private: params[:photo][:private], image: params[:photo][:image],description: params[:photo][:description])
			flash[:notice] = "Photo edited successfully!"
			@photo = Photo.all.new_first.page(params[:page]).per(8)
			render "index"
		elsif params[:photo][:images] == nil && @photo_admin.title == params[:photo][:title]  && @photo_admin.description == params[:photo][:description]  && @photo_admin.private.to_s == params[:photo][:private]
			@photo_admin.update(title: params[:photo][:title], private: params[:photo][:private], description: params[:photo][:description])
			flash[:notice] = "Unchanged through time"
			@photo = Photo.all.new_first.page(params[:page]).per(8)
			render "index"
		else 
			@photo_admin.update(title: params[:photo][:title], private: params[:photo][:private], description: params[:photo][:description])
			flash[:alert] = "#{@photo.errors.full_messages.join(', ')}"
			redirect_to edit_admin_photo_path(@photo_admin)
		end
	end

	def destroy
		@photo_admin.destroy
		@photo = Photo.all.new_first.page(params[:page]).per(8)
		flash[:notice] = "Photo delete successfully!"
		render "index"
	end

	private

	def set_photo
	    @photo_admin = Photo.find_by(id: params[:id])
	end

	def photo_params
		params.require(:photo).permit( :title, :private, :image, :description )
	end
end
