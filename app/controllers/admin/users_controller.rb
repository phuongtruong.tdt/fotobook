class Admin::UsersController < ApplicationController
	before_action :authenticate_user!
	before_action :set_user
	def index 
		@userall = User.all.new_first.page(params[:page]).per(25)
	end

	def edit
		
	end

	def show
		@userall = User.all.new_first.page(params[:page]).per(25)
		render "index"
	end

	def update
		if params[:user][:avatar] != nil
			@member.update(first_name: params[:user][:first_name], last_name: params[:user][:last_name], email: params[:user][:email], avatar: params[:user][:avatar] )	
			@userall = User.all.new_first.page(params[:page]).per(25)
			flash[:notice] = "Users updated successfully!"
			@userall = User.all.new_first.page(params[:page]).per(25)
			render "index"
		elsif params[:user][:avatar] == nil && @member.first_name == params[:user][:first_name] && @member.last_name == params[:user][:last_name] && @member.email == params[:user][:email]
			@member.update(first_name: params[:user][:first_name], last_name: params[:user][:last_name], email: params[:user][:email])	
			@userall = User.all.new_first.page(params[:page]).per(25)
			flash[:notice] = "Unchanged through time"
			@userall = User.all.new_first.page(params[:page]).per(25)
			render "index"
		else 
			@member.update(first_name: params[:user][:first_name], last_name: params[:user][:last_name], email: params[:user][:email] )	
			flash[:alert] = "#{@member.errors.full_messages.join(', ')}"
			redirect_to edit_admin_user_path(@member)
		end
	end

	def destroy
		User.find(params[:id]).destroy
		@userall = User.all.new_first.page(params[:page]).per(25)
		flash[:notice] = "User delete successfully!"
		render "index"
	end

	private
	def set_user
		@member = User.find_by(id: params[:id])
	end
end
