class Admin::AlbumsController < ApplicationController
	before_action :authenticate_user!
	before_action :set_album

	def index
		@album_manage = Album.new_first.page(params[:page]).per(8)
	end

 	def edit
	end

	def update
		set_album()
		if params[:album][:images] != nil
			@album_admin.update(title: params[:album][:title], private: params[:album][:private], images: params[:album][:images],description: params[:album][:description])
			flash[:notice] = "Album edited successfully!"
			@album_manage = Album.new_first.page(params[:page]).per(8)
			render "index"
		elsif params[:album][:images] == nil && @album_admin.title == params[:album][:title]  && @album_admin.description == params[:album][:description] && @album_admin.private.to_s == params[:album][:private] && @album_admin.images.size > 1
			
			@album_admin.update(title: params[:album][:title], private: params[:album][:private], description: params[:album][:description])
			flash[:notice] = "Unchanged through time!"
			@album_manage = Album.new_first.page(params[:page]).per(8)
			render "index"
		else 
			@album_admin.update(title: params[:album][:title], private: params[:album][:private], description: params[:album][:description])
			flash[:alert] = "#{@album_admin.errors.full_messages.join(', ')}"
			redirect_to edit_admin_album_path(@album_admin)
		end
	end

	def destroy
		@album_admin.destroy
		@album_manage = Photo.all.new_first.page(params[:page]).per(8)
		flash[:notice] = "Album delete successfully!"
		render "index"
	end

	def delete_images
	  	@album_admin.images.purge
	  	redirect_to edit_admin_album_path(@album_admin)
	end
	def delete_image_attachment
		@new_album = Album.find(params[:album_id])
	 	@image = @new_album.images.find_by(id: params[:image_id]).purge
	 	redirect_to edit_admin_album_path(@new_album)
	end
 
  private
	def set_album
	    @album_admin = Album.find_by(id: params[:id])
	end
end
