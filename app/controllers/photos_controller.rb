class PhotosController < ApplicationController
	before_action :authenticate_user!
	before_action :set_photo, only: [:show, :edit, :update, :destroy]
	def feed_photos
		list_follow = current_user.following.ids
		@photo_manage = Photo.new_first.where(private: false, user_id: list_follow).page(params[:page]).per(6)
	end

	def discover_photos
		@photo_manage = Photo.new_first.where(private: false).page(params[:page]).per(6)
	end

	def show
    	@photo = Photo.find(params[:id])
 	end

	def new
	   	@photo = Photo.new
	end

	def create
	    @photo = current_user.photos.new(photo_params)
	   	if @photo.save && @photo.image.attached?
	    	flash[:notice] = "Photo created successfully!"
	    	redirect_to current_user
	  	elsif
	  		if @photo.image.attached? == false
				flash[:alert] = "Please choose your image"
				render "new"
	  		else
	    		flash[:alert] = "#{@photo.errors.full_messages.join(', ')}"
	    		render  "new"
	    	end
	 	end
	end

	def edit
	end

	def update
		if params[:photo][:private] != @photo.private.to_s || params[:photo][:title] != @photo.title || params[:photo][:description] != @photo.description
			if params[:photo][:image] != nil
				@photo.update(title: params[:photo][:title], private: params[:photo][:private], image: params[:photo][:image],description: params[:photo][:description])
			else 	
				@photo.update(title: params[:photo][:title], private: params[:photo][:private], description: params[:photo][:description])
			end
			flash[:notice] = "Photo edited successfully!"
			redirect_to current_user
		elsif params[:photo][:images] == nil && @photo.title == params[:photo][:title]  && @photo.description == params[:photo][:description]  && @photo.private.to_s == params[:photo][:private]
			@photo.update(title: params[:photo][:title], private: params[:photo][:private], description: params[:photo][:description])
			flash[:notice] = "Unchanged through time"
			redirect_to current_user
		else 
			flash[:alert] = "#{@photo.errors.full_messages.join(', ')}"
			redirect_to edit_photo_path(@photo)
		end
	end

	def destroy
		@photo.destroy
		flash[:notice] = "Photo delete successfully!"
		redirect_to current_user
	end

	private

	def set_photo
	    @photo = Photo.find_by(id: params[:id])
	end

	def photo_params
		params.require(:photo).permit( :title, :private, :image, :description )
	end
 
end
