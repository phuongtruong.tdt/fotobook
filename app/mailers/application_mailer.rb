class ApplicationMailer < ActionMailer::Base
  default from: 'fotobook@gmail.com'
  layout 'mailer'
end
